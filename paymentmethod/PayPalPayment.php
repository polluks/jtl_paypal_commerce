<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod;

use DateTime;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Alert\Alert;
use JTL\Backend\NotificationEntry;
use JTL\Cart\Cart;
use JTL\Checkout\Bestellung;
use JTL\Checkout\Lieferadresse;
use JTL\Customer\Customer;
use JTL\DB\ReturnType;
use JTL\Helpers\Request;
use JTL\Helpers\Text;
use JTL\Link\LinkInterface;
use JTL\Mail\Mail\Mail;
use JTL\Mail\Mailer;
use JTL\Plugin\Data\PaymentMethod;
use JTL\Plugin\Helper as PluginHelper;
use JTL\Plugin\Payment\Method;
use JTL\Plugin\PluginInterface;
use JTL\Plugin\State;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\adminmenu\Renderer;
use Plugin\jtl_paypal_commerce\frontend\Handler;
use Plugin\jtl_paypal_commerce\paymentmethod\PPCP\OrderNotFoundException;
use Plugin\jtl_paypal_commerce\paymentmethod\PPCP\PPCPOrder;
use Plugin\jtl_paypal_commerce\paymentmethod\PPCP\PPCPOrderInterface;
use Plugin\jtl_paypal_commerce\paymentmethod\TestCase\TCCaptureDecline;
use Plugin\jtl_paypal_commerce\paymentmethod\TestCase\TCCapturePending;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\HttpClient\PPCClient;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\MerchantIntegrationRequest;
use Plugin\jtl_paypal_commerce\PPC\Onboarding\MerchantIntegrationResponse;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\AmountWithBreakdown;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Capture;
use Plugin\jtl_paypal_commerce\PPC\Order\Order;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AuthResult;
use Plugin\jtl_paypal_commerce\PPC\Order\Phone;
use Plugin\jtl_paypal_commerce\PPC\Order\Purchase\PurchaseUnit;
use Plugin\jtl_paypal_commerce\PPC\Order\Shipping;
use Plugin\jtl_paypal_commerce\PPC\Order\Transaction;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\ClientErrorResponse;
use Plugin\jtl_paypal_commerce\PPC\Request\PPCRequestException;
use Plugin\jtl_paypal_commerce\PPC\Webhook\EventType;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class PayPalPayment
 * @package Plugin\jtl_paypal_commerce\paymentmethod
 */
abstract class PayPalPayment extends Method implements PayPalPaymentInterface
{
    /** @var PaymentMethod */
    protected PaymentMethod $method;

    /** @var PluginInterface|null */
    protected ?PluginInterface $plugin;

    /** @var array */
    protected array $localizations = [];

    /** @var Configuration|null */
    protected ?Configuration $config = null;

    /** @var PaymentSession */
    protected PaymentSession $sessionCache;

    /** @var Helper */
    protected Helper $helper;

    /** @var PPCPOrderInterface|null */
    protected ?PPCPOrderInterface $ppcpOrder = null;

    /**
     * @inheritDoc
     */
    public function init(int $nAgainCheckout = 0)
    {
        parent::init($nAgainCheckout);

        $this->plugin = PluginHelper::getPluginById('jtl_paypal_commerce');
        if ($this->plugin === null) {
            return $this;
        }

        $this->method       = $this->plugin->getPaymentMethods()->getMethodByID($this->moduleID);
        $this->kZahlungsart = $this->method->getMethodID();
        $this->name         = $this->method->getName();
        $this->sessionCache = PaymentSession::instance($this->moduleID);
        $this->helper       = Helper::getInstance($this->plugin)->setLogger(new Logger(Logger::TYPE_PAYMENT, $this));

        if ($this->plugin->getState() !== State::ACTIVATED) {
            return $this;
        }

        $this->config = PPCHelper::getConfiguration($this->plugin);

        foreach ($this->method->getLocalization() as $localization) {
            $this->localizations[$localization->cISOSprache] = $localization;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isValidIntern(array $args_arr = []): bool
    {
        if (!parent::isValidIntern($args_arr)) {
            return false;
        }

        if ($this->config === null || $this->plugin === null || $this->plugin->getState() !== State::ACTIVATED) {
            return false;
        }

        return true;
    }

    /**
     * @param Order  $order
     * @param string $state
     * @return bool
     */
    protected function isValidOrderState(Order $order, string $state): bool
    {
        $orderState   = $order->getStatus();
        $capture      = $order->getPurchase()->getCapture();
        $captureState = $capture !== null ? $capture->getStatus() : OrderStatus::STATUS_UNKONWN;

        return match ($state) {
            OrderStatus::STATUS_CREATED   => \in_array($orderState, [
                OrderStatus::STATUS_CREATED,
                OrderStatus::STATUS_APPROVED
            ], true),
            OrderStatus::STATUS_COMPLETED => $orderState === OrderStatus::STATUS_COMPLETED
                && $captureState === $orderState,
            OrderStatus::STATUS_PENDING => \in_array($orderState, [
                    OrderStatus::STATUS_PENDING,
                    OrderStatus::STATUS_PENDING_APPROVAL
                ], true) || $captureState === OrderStatus::STATUS_PENDING,
            default => $orderState === $state
                || ($orderState === OrderStatus::STATUS_COMPLETED && $captureState === $state),
        };
    }

    /**
     * @inheritDoc
     */
    public function mappedLocalizedPaymentName(?string $isoCode = null): string
    {
        return $this->getLocalizedPaymentName($isoCode);
    }

    /**
     * @inheritDoc
     */
    public function getLocalizedPaymentName(?string $isoCode = null): string
    {
        if ($isoCode === null) {
            $isoCode = Shop::getLanguageCode();
        }

        if (!isset($this->localizations[$isoCode])) {
            return $this->getMethod()->getName();
        }

        return $this->localizations[$isoCode]->cName;
    }

    /**
     * @inheritDoc
     */
    public function getBackendNotification(PluginInterface $plugin, bool $force = false): ?NotificationEntry
    {
        if (!$this->method->getDuringOrder() && !$this->paymentAfterOrderSupported()
            && ($this->isAssigned() || $force)
        ) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                \__($this->method->getName()),
                \__('Zahlung nach Bestellabschluß wird nicht unterstützt.'),
                Shop::getAdminURL() . '/zahlungsarten.php?kZahlungsart=' . $this->method->getMethodID()
                . '&token=' . $_SESSION['jtl_token']
            );
            $entry->setPluginId($plugin->getPluginID());

            return $entry;
        }

        if ($this->method->getDuringOrder() && !$this->paymentDuringOrderSupported()
            && ($this->isAssigned() || $force)
        ) {
            $entry = new NotificationEntry(
                NotificationEntry::TYPE_WARNING,
                \__($this->method->getName()),
                \__('Zahlung vor Bestellabschluß wird nicht unterstützt.'),
                Shop::getAdminURL() . '/zahlungsarten.php?kZahlungsart=' . $this->method->getMethodID()
                . '&token=' . $_SESSION['jtl_token']
            );
            $entry->setPluginId($plugin->getPluginID());

            return $entry;
        }

        return null;
    }

    /**
     * @return bool
     */
    abstract protected function usePurchaseItems(): bool;

    /**
     * @param object $paymentSession
     * @return bool
     */
    abstract protected function isSessionPayed(object $paymentSession): bool;

    /**
     * @return string|null
     */
    protected function getCUID(): ?string
    {
        $payment = Shop::Container()->getDB()->queryPrepared(
            'SELECT tbestellstatus.cUID
                FROM tzahlungsession
                INNER JOIN tbestellstatus ON tzahlungsession.kBestellung = tbestellstatus.kBestellung
                WHERE tzahlungsession.cSID = :sessID
                ORDER BY tzahlungsession.dZeitBezahlt DESC',
            [
                'sessID' => \session_id(),
            ],
            ReturnType::SINGLE_OBJECT
        );

        return $payment->cUID ?? null;
    }

    /**
     * @param Configuration $config
     * @return MerchantIntegrationResponse|null
     */
    protected function getMerchantIntegration(Configuration $config): ?MerchantIntegrationResponse
    {
        $client      = new PPCClient(PPCHelper::getEnvironment($this->config));
        $workingMode = $config->getWorkingMode();
        $merchantID  = $config->getPrefixedConfigItem('merchantID_' . $workingMode);
        $partnerID   = \base64_decode(MerchantCredentials::partnerID($workingMode));

        try {
            return new MerchantIntegrationResponse($client->send(new MerchantIntegrationRequest(
                Token::getInstance()->getToken(),
                $partnerID,
                $merchantID ?? ''
            )));
        } catch (GuzzleException | AuthorizationException | PPCRequestException $e) {
            $this->helper->logWrite(\LOGLEVEL_ERROR, 'MerchantIntegrationRequest: ' . $e->getMessage(), $e);
        }

        return null;
    }

    /**
     * @param Customer $customer
     * @param int      $needed
     * @return Payer
     */
    protected function createPayer(Customer $customer, int $needed = Payer::PAYER_DEFAULT): Payer
    {
        $payer = new Payer();
        if (($needed & Payer::PAYER_NAME) > 0) {
            if (!empty($customer->cNachname)) {
                $payer->setSurname(Text::unhtmlentities($customer->cNachname));
            }
            if (!empty($customer->cVorname)) {
                $payer->setGivenName(Text::unhtmlentities($customer->cVorname));
            }
        }
        if (($needed & Payer::PAYER_EMAIL) > 0 && !empty($customer->cMail)) {
            $payer->setEmail($customer->cMail);
        }
        if (($needed & Payer::PAYER_LOCATION) > 0) {
            $payer->setAddress(Address::createFromCustomer($customer));
        }

        if (($needed & Payer::PAYER_BIRTH) > 0) {
            try {
                $payer->setBirthDate(!empty($customer->dGeburtstag) && $customer->dGeburtstag !== '0000-00-00'
                    ? new DateTime($customer->dGeburtstag)
                    : null);
            } catch (Exception) {
                $payer->setBirthDate(null);
            }
        }

        if (($needed & Payer::PAYER_PHONE) > 0) {
            if (!empty($customer->cTel)) {
                $payer->setPhone((new Phone())->setNumber($customer->cTel));
            } elseif (!empty($customer->cMobil)) {
                $payer->setPhone((new Phone())->setNumber($customer->cMobil));
            }
        }

        return $payer;
    }

    /**
     * @param int    $languageID
     * @param string $sppContext
     * @param string $payAction
     * @return AppContext
     */
    protected function createAppContext(
        int $languageID,
        string $sppContext = AppContext::SHIPPING_PROVIDED,
        string $payAction = AppContext::PAY_NOW
    ): AppContext {
        return (new AppContext())
            ->setLocale(Helper::getLocaleFromISO(
                Helper::sanitizeISOCode(Shop::Lang()->getIsoFromLangID($languageID)->cISO)
            ))
            ->setBrandName($this->getShopTitle())
            ->setShipping($sppContext)
            ->setPayAction($payAction);
    }

    /**
     * @param string        $orderHash
     * @param Lieferadresse $shipping
     * @param Cart          $cart
     * @return PurchaseUnit
     */
    protected function createPurchase(
        string $orderHash,
        Lieferadresse $shipping,
        Cart $cart
    ): PurchaseUnit {
        $currency = Frontend::getCurrency();
        $merchant = Frontend::getCustomerGroup()->isMerchant();
        $amount   = AmountWithBreakdown::createFromCart($cart, $currency->getCode(), $merchant);
        $purchase = (new PurchaseUnit())
            ->setReferenceId(PurchaseUnit::REFERENCE_DEFAULT)
            ->setAmount($amount)
            ->setCustomId($orderHash)
            ->setDescription($this->helper->getDescriptionFromCart($cart))
            ->setShipping((new Shipping())
                ->setName(
                    \trim(Text::unhtmlentities($shipping->cVorname) . ' ' . Text::unhtmlentities($shipping->cNachname))
                )
                ->setAddress(Address::createFromOrderAddress($shipping)));

        if ($this->usePurchaseItems()) {
            $purchase->addItemsFromCart($cart, $currency, $amount);
        }

        return $purchase;
    }

    /**
     * @param Customer $customer
     * @param Cart     $cart
     * @param string   $shippingContext
     * @param string   $payAction
     * @param string   $orderHash
     * @return Order
     */
    protected function constructOrder(
        Customer $customer,
        Cart $cart,
        string $shippingContext,
        string $payAction,
        string $orderHash
    ): Order {
        return (new Order())
            ->setPayer($this->createPayer($customer))
            ->addPurchase($this->createPurchase($orderHash, Frontend::getDeliveryAddress(), $cart))
            ->setAppContext($this->createAppContext(
                $customer->kSprache ?? Shop::Lang()->currentLanguageID,
                $shippingContext,
                $payAction
            ))
            ->setIntent(Order::INTENT_CAPTURE);
    }

    /**
     * @param Order  $createOrder
     * @param string $bnCode
     * @return PPCPOrderInterface
     * @throws PPCRequestException
     */
    protected function createPPCPOrder(Order $createOrder, string $bnCode): PPCPOrderInterface
    {
        return ($this->ppcpOrder = PPCPOrder::create($createOrder, $bnCode, $this->helper));
    }

    /**
     * @param string $orderId
     * @return PPCPOrderInterface
     * @throws OrderNotFoundException | PPCRequestException
     */
    protected function loadPPCPOrder(string $orderId): PPCPOrderInterface
    {
        return ($this->ppcpOrder = PPCPOrder::load($orderId, $this->helper));
    }

    /**
     * @param Order  $ppOrder
     * @param string $bnCode
     * @return Order
     * @throws PPCRequestException | OrderNotFoundException
     */
    protected function createOrder(
        Order $ppOrder,
        string $bnCode = MerchantCredentials::BNCODE_CHECKOUT
    ): Order {
        $this->helper->logWrite(\LOGLEVEL_DEBUG, 'createOrder:', $ppOrder);
        $ppOrder = $this->createPPCPOrder($ppOrder, $bnCode)->callGet();
        $this->storePPOrder($ppOrder);

        return $ppOrder;
    }

    /**
     * @inheritDoc
     */
    public function verifyPPOrder(string $orderId): Order
    {
        $ppOrder = $this->ppcpOrder === null
            ? $this->loadPPCPOrder($orderId)->callGet()
            : $this->ppcpOrder->callGet($orderId, true);
        $this->storePPOrder($ppOrder);

        return $ppOrder;
    }

    /**
     * @param string $fundingSource
     * @return string
     */
    protected function validateFundingSource(string $fundingSource): string
    {
        if ($fundingSource === '') {
            return $this->getFundingSource();
        }
        if ($fundingSource !== $this->getFundingSource()) {
            $this->unsetCache();
            $this->setFundingSource($fundingSource);
        }

        return $fundingSource;
    }

    /**
     * @inheritDoc
     */
    public function validatePaymentConfiguration(PaymentMethod $method): bool
    {
        if ($this->paymentDuringOrderSupported()
            && Request::postInt('nWaehrendBestellung', $method->getDuringOrder() ? 1 : 0) === 1
        ) {
            return true;
        }
        if ($this->paymentAfterOrderSupported()
            && Request::postInt('nWaehrendBestellung', $method->getDuringOrder() ? 1 : 0) === 0) {
            return true;
        }

        Shop::Container()->getAlertService()->addAlert(
            Alert::TYPE_WARNING,
            $this->paymentDuringOrderSupported()
                ? \sprintf(\__('Zahlung nach Bestellabschluß wird von %s nicht unterstützt.'), $method->getName())
                : \sprintf(\__('Zahlung vor Bestellabschluß wird von %s nicht unterstützt.'), $method->getName()),
            'duringCheckoutNotSupported'
        );
        if (isset($_POST['nWaehrendBestellung']) && Request::postInt('einstellungen_bearbeiten') > 0) {
            $_POST['nWaehrendBestellung'] = $this->paymentDuringOrderSupported() ? '1' : '0';
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function validatePayerData(Customer $customer, ?Lieferadresse $shippingAdr = null, ?Cart $cart = null): void
    {
    }

    /**
     * @inheritDoc
     */
    public function setFundingSource(string $fundingSource): void
    {
        if ($fundingSource !== '') {
            $this->sessionCache->setFundingSource($fundingSource);
        }
    }

    /**
     * @inheritDoc
     */
    public function getFundingSource(): string
    {
        return $this->sessionCache->getFundingSource() ?? '';
    }

    /**
     * @inheritDoc
     */
    public function setBNCode(string $bnCode = MerchantCredentials::BNCODE_CHECKOUT): void
    {
        $this->sessionCache->setBNCode($bnCode);
    }

    /**
     * @inheritDoc
     */
    public function getBNCode(string $default = MerchantCredentials::BNCODE_CHECKOUT): string
    {
        return $this->sessionCache->getBNCode($default);
    }

    /**
     * @inheritDoc
     */
    public function renderBackendInformation(JTLSmarty $smarty, PluginInterface $plugin, Renderer $renderer): void
    {
        // nothing do to...
    }

    /**
     * @inheritDoc
     */
    public function isAssigned(string $shippingClasses = '', int $customerGroupID = 0, int $shippingMethodID = 0): bool
    {
        $key = $shippingClasses . '_' . $customerGroupID . '_' . $shippingMethodID;
        static $assigned;

        if (($assigned[$key] ?? null) === null) {
            $queryShipping      = '';
            $queryCustomerGroup = '';
            $params             = [
                'paymentID' => $this->getMethod()->getMethodID(),
            ];
            if ($shippingClasses !== '') {
                $queryShipping = "AND (tversandart.cVersandklassen = '-1'
                    OR tversandart.cVersandklassen LIKE :shippingClass1
                    OR tversandart.cVersandklassen LIKE :shippingClass2
                )";

                $params['shippingClass1'] = '% ' . $shippingClasses . ' %';
                $params['shippingClass2'] = '% ' . $shippingClasses;
            }
            if ($customerGroupID > 0) {
                $queryCustomerGroup = "AND (tversandart.cKundengruppen = '-1'
                    OR tversandart.cKundengruppen LIKE :customerGroup
                )";

                $params['customerGroup'] = '%;' . $customerGroupID . ';%';
            }
            if ($shippingMethodID > 0) {
                $queryShipping       .= ' AND tversandartzahlungsart.kVersandart = :shippingID';
                $params['shippingID'] = $shippingMethodID;
            }

            $result         = Shop::Container()->getDB()->getSingleObject(
                'SELECT COUNT(tversandart.kVersandart) AS cnt
                    FROM tversandartzahlungsart
                    INNER JOIN tversandart
                        ON tversandartzahlungsart.kVersandart = tversandart.kVersandart
                    WHERE tversandartzahlungsart.kZahlungsart = :paymentID
                    ' . $queryShipping . '
                    ' . $queryCustomerGroup,
                $params
            );
            $assigned[$key] = $result && (int)$result->cnt > 0;
        }

        return $assigned[$key];
    }

    /**
     * @inheritDoc
     */
    public function getMethod(): PaymentMethod
    {
        return $this->method;
    }

    /**
     * @inheritDoc
     */
    public function storePPOrder(?Order $order): void
    {
        if ($order === null) {
            $this->sessionCache->clear(PaymentSession::ORDERID);
            $this->ppcpOrder = null;

            return;
        }

        $this->sessionCache->setOrderId($order->getId());
        Shop::Container()->getDB()->update('tzahlungsid', 'cId', $this->helper->getSanitizedHash($order), (object)[
            'txn_id' => $order->getId(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function resetPPOrder(?string $orderId = null): void
    {
        if ($orderId !== null) {
            $ppOrder = $this->getPPOrder($orderId);
            if ($ppOrder !== null && $this->sessionCache->getOrderId() === $ppOrder->getId()) {
                $this->sessionCache->clear(PaymentSession::ORDERID);
                $this->ppcpOrder = null;
            }
        } else {
            $this->unsetCache();
        }
    }

    /**
     * @inheritDoc
     */
    public function getPPOrder(?string $orderId = null): ?Order
    {
        $orderId = $orderId ?? $this->sessionCache->getOrderId();
        if ($this->ppcpOrder === null) {
            if (empty($orderId)) {
                return null;
            }

            try {
                return $this->loadPPCPOrder($orderId)->callGet();
            } catch (PPCRequestException | OrderNotFoundException) {
                return null;
            }
        }

        try {
            return empty($orderId) ? $this->ppcpOrder->callGet() : $this->ppcpOrder->callGet($orderId);
        } catch (PPCRequestException | OrderNotFoundException) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function createPPOrder(
        Customer $customer,
        Cart     $cart,
        string   $fundingSource,
        string   $shippingContext,
        string   $payAction,
        string   $bnCode = MerchantCredentials::BNCODE_CHECKOUT
    ): ?string {
        $fundingSource = $this->validateFundingSource($fundingSource);
        if ($fundingSource === '') {
            $this->helper->logWrite(\LOGLEVEL_ERROR, 'createPPOrder: funding source is empty');

            return null;
        }

        $ppOrderId = $this->sessionCache->getOrderId();
        $orderHash = $this->generateHash(new Bestellung());
        $ppOrder   = $this->constructOrder($customer, $cart, $shippingContext, $payAction, $orderHash);
        $this->sessionCache->setOrderHash($orderHash);
        if (!empty($ppOrderId)) {
            $ppOrder->setId($ppOrderId);
        }

        $transaction = Transaction::instance();
        $transaction->startTransaction(Transaction::CONTEXT_CREATE);

        try {
            $ppOrder = $this->createOrder($ppOrder, $bnCode);
            $this->helper->logWrite(\LOGLEVEL_DEBUG, 'createPPOrder: createOrderResponse', $ppOrder);
            $transaction->clearTransaction(Transaction::CONTEXT_CREATE);
            if (!$this->isValidOrderState($ppOrder, OrderStatus::STATUS_CREATED)) {
                $this->helper->logWrite(\LOGLEVEL_NOTICE, 'createPPOrder: UnexpectedOrderState get '
                    . $ppOrder->getStatus() . ' expected ' . OrderStatus::STATUS_CREATED);

                return null;
            }

            return $ppOrder->getId();
        } catch (PPCRequestException $e) {
            $this->unsetCache();
            $this->helper->logWrite(\LOGLEVEL_NOTICE, 'createPPOrder: ' . $e->getName(), $e);
            $this->showErrorResponse($e->getResponse(), new Alert(
                Alert::TYPE_ERROR,
                Handler::getBackendTranslation($e->getMessage()),
                'createOrderRequest',
                ['saveInSession' => true]
            ));
        } catch (OrderNotFoundException) {
            $this->unsetCache();
            $this->helper->logWrite(\LOGLEVEL_NOTICE, 'createPPOrder: created order could not be found');
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_error'),
                'preparePaymentProcess',
                ['saveInSession' => true]
            );
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function get3DSAuthResult(string $authAction): string
    {
        if (\in_array($authAction, [AuthResult::AUTHACTION_CONTINUE, AuthResult::AUTHACTION_REJECT], true)) {
            return $authAction;
        }

        $prefix = BackendUIsettings::BACKEND_SETTINGS_SECTION_ACDCDISPLAY . '_';

        return match ($authAction) {
            AuthResult::AUTHACTION_ERROR,
            AuthResult::AUTHACTION_CANCEL => $this->config->getPrefixedConfigItem(
                $prefix . $authAction,
                AuthResult::AUTHACTION_REJECT
            ),
            AuthResult::AUTHACTION_SKIP,
            AuthResult::AUTHACTION_NOTSUPPORTED,
            AuthResult::AUTHACTION_UNABLETOCOMPLETE,
            AuthResult::AUTHACTION_NOTELIGIBLE => $this->config->getPrefixedConfigItem(
                $prefix . $authAction,
                AuthResult::AUTHACTION_CONTINUE
            ),
            default => AuthResult::AUTHACTION_REJECT,
        };
    }

    /**
     * @inheritDoc
     */
    public function getAssignedPayments(Bestellung $shopOrder): array
    {
        $result = [];
        $db     = Shop::Container()->getDB();
        foreach ($db->getObjects(
            'SELECT tbestellung.kBestellung, tbestellung.cBestellNr,
                   tzahlungsid.kZahlungsart, tzahlungsid.txn_id
                FROM tbestellung
                LEFT JOIN tzahlungseingang ON tzahlungseingang.kBestellung = tbestellung.kBestellung
                LEFT JOIN tzahlungsid ON tzahlungsid.kBestellung = tbestellung.kBestellung
                WHERE tbestellung.kBestellung = :orderId
                    AND tzahlungsid.kZahlungsart = :paymentId',
            [
                'orderId'   => $shopOrder->kBestellung,
                'paymentId' => $this->getMethod()->getMethodID(),
            ]
        ) as $payment) {
            $orderId = $payment->txn_id;
            if (empty($orderId)) {
                continue;
            }
            try {
                $order = (new TCCaptureDecline())->execute(
                    $this,
                    (new TCCapturePending())->execute(
                        $this,
                        PPCPOrder::load($orderId, $this->helper)->callGet()
                    )
                );
            } catch (PPCRequestException | OrderNotFoundException) {
                continue;
            }
            $capture = $order->getPurchase()->getCapture();
            if ($capture === null) {
                continue;
            }
            $result[$orderId] = AssignedPayment::load($capture);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function updatePaymentState(string $paymentHash, Bestellung $order): void
    {
        $db = Shop::Container()->getDB();
        $db->update('tzahlungsession', 'cZahlungsID', $paymentHash, (object)[
            'kBestellung'  => $order->kBestellung,
            'cSID'         => \session_id(),
        ]);
        $db->update('tzahlungsid', 'cId', $paymentHash, (object)[
            'kBestellung' => $order->kBestellung,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getPaymentStateURL(): ?string
    {
        /** @var LinkInterface $stateLink */
        $stateLink = $this->plugin->getLinks()->getLinks()->first(static function (LinkInterface $link) {
            return $link->getTemplate() === 'pendingpayment.tpl';
        });

        return $stateLink !== null ? $stateLink->getURL() . '?payment=' . $this->getMethod()->getMethodID() : null;
    }

    /**
     * @inheritDoc
     */
    public function getPaymentCancelURL(): string
    {
        return Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php') . '?editZahlungsart=1';
    }

    /**
     * @inheritDoc
     */
    public function getPaymentRetryURL(): string
    {
        return Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php');
    }

    /**
     * @inheritDoc
     */
    public function getValidOrderState(Order $order): string
    {
        $orderState   = $order->getStatus();
        $capture      = $order->getPurchase()->getCapture();
        $captureState = $capture !== null ? $capture->getStatus() : null;

        $state = $orderState === OrderStatus::STATUS_COMPLETED ? $captureState ?? $orderState : $orderState;

        return $state === OrderStatus::STATUS_PENDING_APPROVAL ? OrderStatus::STATUS_PENDING : $state;
    }

    /**
     * @inheritDoc
     */
    public function unsetCache(?string $cKey = null)
    {
        $this->sessionCache->clear($cKey);

        return parent::unsetCache($cKey);
    }

    /**
     * @inheritDoc
     */
    public function generateHash(Bestellung $order): string
    {
        $hash = parent::generateHash($order);
        $hash = \str_starts_with($hash, '_') ? \substr($hash, 1) : $hash;
        $db   = Shop::Container()->getDB();
        $db->delete('tzahlungsid', 'cId', $hash);
        $db->insert('tzahlungsid', (object)[
            'kBestellung'  => $order->kBestellung ?? 0,
            'kZahlungsart' => $this->getMethod()->getMethodID(),
            'cId'          => $hash,
            'txn_id'       => '',
            'dDatum'       => 'NOW()',
        ]);

        return $hash;
    }

    /**
     * @inheritDoc
     */
    public function redirectOnPaymentSuccess(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getReturnURL(Bestellung $order): string
    {
        if (Shop::getSettings([\CONF_KAUFABWICKLUNG])['kaufabwicklung']['bestellabschluss_abschlussseite'] === 'A') {
            $oZahlungsID = Shop::Container()->getDB()->queryPrepared(
                'SELECT cId
                    FROM tbestellid
                    WHERE kBestellung = :orderId',
                [
                    'orderId' => $order->kBestellung,
                ],
                ReturnType::SINGLE_OBJECT
            );
            if (\is_object($oZahlungsID)) {
                return Shop::getURL() . '/bestellabschluss.php?i=' . $oZahlungsID->cId;
            }
        }

        if (empty($order->BestellstatusURL)) {
            $cUID = $this->getCUID();

            return $cUID !== null
                ? Shop::Container()->getLinkService()->getStaticRoute('status.php') . '?uid=' . $cUID
                : Shop::Container()->getLinkService()->getStaticRoute('jtl.php') . '?bestellungen=1';
        }

        return $order->BestellstatusURL;
    }

    /**
     * @inheritDoc
     */
    public function addIncomingPayment(Bestellung $order, object $payment)
    {
        $payData = $payment;

        $order->cZahlungsartName = $this->getMethod()->getName();
        if ($payment instanceof Order) {
            $capture = $payment->getPurchase()->getCapture() ?? new Capture();
            $amount  = $capture->getAmount();
            $fee     = $amount->getBreakdownItem('paypal_fee');
            $payer   = $payment->getPayer();
            $payData = (object)[
                'fBetrag'           => $amount->getValue(),
                'fZahlungsgebuehr'  => $fee !== null ? $fee->getValue() : 0,
                'cISO'              => $amount->getCurrencyCode(),
                'cZahler'           => $payer !== null ? $payer->getSurname() . ', ' . $payer->getEmail() : '',
                'cHinweis'          => $capture->getId(),
            ];
        }

        return parent::addIncomingPayment($order, $payData);
    }

    /**
     * @inheritDoc
     */
    public function setOrderStatusToPaid(Bestellung $order)
    {
        $paySum = Shop::Container()->getDB()->getSingleObject(
            'SELECT SUM(fBetrag) AS incommingSum
                FROM tzahlungseingang
                WHERE kBestellung = :orderId',
            ['orderId' => $order->kBestellung]
        );
        if ($paySum !== null && (float)$order->fGesamtsumme > (float)$paySum->incommingSum) {
            return $this;
        }

        Shop::Container()->getDB()->update(
            'tbestellung',
            'kBestellung',
            $order->kBestellung,
            (object)[
                'cStatus'          => \BESTELLUNG_STATUS_BEZAHLT,
                'dBezahltDatum'    => 'NOW()',
                'cZahlungsartName' => $this->getMethod()->getName(),
            ]
        );

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function finalizeOrder(Bestellung $order, string $hash, array $args): bool
    {
        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null) {
            $ppOrder = new Order();
            $ppOrder->addPurchase((new PurchaseUnit())
                ->setInvoiceId($order->cBestellNr));
        }
        $invoiceId = $ppOrder->getPurchase()->getInvoiceId();
        if (empty($invoiceId)) {
            $invoiceId = $args['invoiceId'] ?? null;
        }

        if ($order->cBestellNr !== $invoiceId && !empty($invoiceId)) {
            $order->cBestellNr = $invoiceId;
        }

        return !$this->helper->existsOrder($order);
    }

    /**
     * @inheritDoc
     */
    public function finalizeOrderInDB(Bestellung $order): void
    {
        $ppOrder = $this->getPPOrder();
        if ($ppOrder !== null) {
            $order->cBestellNr = $ppOrder->getInvoiceId();
        }
        // prevent wawi synchronisation on a pending order
        $order->cAbgeholt = 'P';
    }

    /**
     * @inheritDoc
     */
    public function sendMail(int $orderID, string $type, $additional = null)
    {
        if (\str_starts_with($type, 'kPlugin_' . $this->plugin->getID())) {
            $order = new Bestellung($orderID);
            $order->fuelleBestellung(false);
            $data = (object)[
                'tkunde'      => new Customer($order->kKunde),
                'tbestellung' => $order,
            ];
            try {
                $mailer = Shop::Container()->get(Mailer::class);
                $mail   = new Mail();
                $mailer->send($mail->createFromTemplateID($type, $data));
            } catch (NotFoundExceptionInterface | ContainerExceptionInterface) {
            }

            return $this;
        }

        return parent::sendMail($orderID, $type, $additional);
    }

    /**
     * @param string|null $msg
     * @param string|null $exitUrl
     * @return void
     */
    protected function raisePaymentError(?string $msg = null, ?string $exitUrl = null): void
    {
        $localMsg = $this->plugin->getLocalization()
                                 ->getTranslation($msg ?? 'jtl_paypal_commerce_payment_error') ?? $msg;

        if ($localMsg !== null) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                $localMsg,
                'paymentError',
                ['saveInSession' => true]
            );
        }

        Helper::redirectAndExit($exitUrl ?? $this->getPaymentCancelURL());
    }

    /**
     * @param ClientErrorResponse $response
     * @param Alert|null          $default
     * @return void
     */
    protected function showErrorResponse(ClientErrorResponse $response, ?Alert $default = null): void
    {
        $alertService = Shop::Container()->getAlertService();
        $errDetail    = $response->getDetail();
        if ($errDetail !== null) {
            $this->helper->logWrite(\LOGLEVEL_NOTICE, $errDetail->getDescription(), $response);
            $issue = $errDetail->getIssue() ?? '';
            $msg   = Handler::getBackendTranslation($issue);
            if ($msg === $issue) {
                $msg = Handler::getBackendTranslation($errDetail->getDescription() ?? '');
            }
            if ($msg !== '') {
                Shop::Container()->getAlertService()->addError(
                    $msg,
                    'createOrderRequest',
                    ['saveInSession' => true]
                );

                return;
            }
        }

        if ($default !== null) {
            $alertService->removeAlertByKey($default->getKey());
            $alertService->getAlertlist()->push($default);
        }
    }

    /**
     * @inheritdoc
     */
    public function handleOrder(Order $order, ?Bestellung $shopOrder = null, bool $return = false): void
    {
        $orderExists = $shopOrder !== null && $this->helper->existsOrder($shopOrder);
        if ($this->isValidOrderState($order, OrderStatus::STATUS_DECLINED)) {
            if (!$orderExists) {
                // payment is declined, no order was created => create shop order and goto success page
                $this->helper->persistOrder($shopOrder, $order, $this, []);
            }
            $this->helper->declinePayment($shopOrder, $order->getPurchase()->getCapture(), $this);
            $this->onPaymentComplete($order);
            $order->setLink((object)[
                'rel'  => 'paymentRedirect',
                'href' => $this->getReturnURL($shopOrder),
            ])->setProcessingInstruction(\sprintf(
                $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_declined'),
                $order->getInvoiceId(),
                $this->getLocalizedPaymentName()
            ));
        } elseif ($this->isValidOrderState($order, OrderStatus::STATUS_PENDING)) {
            if (!$orderExists) {
                // payment is pending but shop order failed => create shop order and goto notification page
                $this->helper->persistOrder($shopOrder, $order, $this, []);
            }
            $capture = $order->getPurchase()->getCapture();
            if ($capture !== null) {
                $this->helper->capturePayment($shopOrder, $capture, $order, $this);
            }
            $order->setLink((object)[
                'rel'  => 'paymentRedirect',
                'href' => $this->getPaymentStateURL(),
            ]);
        } elseif ($this->isValidOrderState($order, OrderStatus::STATUS_COMPLETED)) {
            if (!$orderExists) {
                // payment is created but shop order failed => create shop order and goto success page
                $this->helper->persistOrder($shopOrder, $order, $this, []);
            }
            $this->helper->capturePayment($shopOrder, $order->getPurchase()->getCapture(), $order, $this);
            $this->onPaymentComplete($order);
            $order->setLink((object)[
                'rel'  => 'paymentRedirect',
                'href' => $this->getReturnURL($shopOrder),
            ]);
        } elseif ($this->isValidOrderState($order, OrderStatus::STATUS_APPROVED)) {
            if ($orderExists) {
                // shop order exist, possible case are paypal server error during capture or approval => goto order page
                $order->setLink((object)[
                    'rel'  => 'paymentRedirect',
                    'href' => $this->getReturnURL($shopOrder),
                ]);
            } else {
                // payment ist created, shop order does not exists => goto order completion page
                $order->setLink((object)[
                    'rel'  => 'paymentRedirect',
                    'href' => $this->getPaymentRetryURL(),
                ]);
            }
        } elseif ($this->isValidOrderState($order, OrderStatus::STATUS_CREATED)) {
            if ($orderExists) {
                // shop order exist, possible case are paypal server error during capture or approval => goto order page
                $order->setLink((object)[
                    'rel'  => 'paymentRedirect',
                    'href' => $this->getReturnURL($shopOrder),
                ]);
            } else {
                // payment ist created, shop order does not exists => goto payment page
                $order->setLink((object)[
                    'rel'  => 'paymentRedirect',
                    'href' => $this->getPaymentCancelURL(),
                ]);
            }
        }

        $this->helper->logWrite(\LOGLEVEL_DEBUG, 'handleOrder', (object)[
            'order'     => $order->getId() . ': ' . $this->getValidOrderState($order),
            'shopOrder' => $orderExists ? $shopOrder->cBestellNr : '',
        ]);

        $processMsg = $order->getProcessingInstruction();
        if (!$return && $processMsg !== '') {
            Shop::Container()->getAlertService()->addInfo($processMsg, 'paymentState', ['saveInSession' => true]);
        }
        $redirect = $order->getLink('paymentRedirect');
        if (!$return && $redirect !== null) {
            Helper::redirectAndExit($redirect);
        }
    }

    /**
     * @inheritDoc
     */
    public function onPendingCapture(): void
    {
        $ppOrder = (new TCCaptureDecline())->execute(
            $this,
            (new TCCapturePending())->execute($this, $this->getPPOrder(), Frontend::getCustomer(), Frontend::getCart()),
            Frontend::getCustomer(),
            Frontend::getCart()
        );
        if ($ppOrder === null) {
            throw new OrderNotFoundException();
        }

        $this->handleOrder($ppOrder, $this->helper->getShopOrder($ppOrder), true);
    }

    /**
     * @inheritDoc
     */
    public function onPaymentState(PaymentStateResult $result, bool $timeOut = false): void
    {
        $ppOrder   = $this->getPPOrder();
        $shopOrder = $ppOrder !== null ? $this->helper->getShopOrder($ppOrder) : null;
        if ($shopOrder !== null && ($result->getState() !== OrderStatus::STATUS_PENDING || $timeOut)) {
            $result->setRedirect($this->getReturnURL($shopOrder));
        }
        // Workaround for session cleanup in case of multiple reloads during capture
        if ($shopOrder !== null
            && $result->getState() === OrderStatus::STATUS_COMPLETED
            && (Frontend::get('ppPersistOrder') ?? 0) === $shopOrder->kBestellung
        ) {
            try {
                Frontend::getInstance(false)->cleanUp();
                Frontend::set('ppPersistOrder', null);
            } catch (Exception $e) {
                $this->helper->logWrite(\LOGLEVEL_ERROR, 'persistOrder: session error (' . $e->getMessage() . ')');
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function onPaymentComplete(Order $order): void
    {
        $shopOrder = $this->helper->getShopOrder($order);
        if ($shopOrder === null) {
            return;
        }

        $this->getDB()->update(
            'tbestellung',
            ['kBestellung', 'cAbgeholt'],
            [$shopOrder->kBestellung, 'P'],
            (object)[
                'cAbgeholt' => 'N',
            ]
        );
        $this->getDB()->update(
            'tzahlungseingang',
            ['kBestellung', 'cZahlungsanbieter', 'cAbgeholt'],
            [$shopOrder->kBestellung, $this->getMethod()->getName(), 'P'],
            (object)[
                'cAbgeholt' => 'N',
            ]
        );

        $this->unsetCache();
    }

    /**
     * @inheritDoc
     */
    public function handleCaptureWebhook(string $eventType, Capture $capture, object $payment): bool
    {
        $this->helper->logWrite(\LOGLEVEL_DEBUG, 'handleCaptureWebhook', $payment);

        $ppOrder   = !empty($payment->txn_id) ? $this->getPPOrder($payment->txn_id) : null;
        $shopOrder = $ppOrder !== null ? $this->helper->getShopOrder($ppOrder) : null;

        if ($ppOrder === null || $shopOrder === null) {
            $this->helper->logWrite(\LOGLEVEL_ERROR, 'handleCaptureWebhook: shop order does not exists', $payment);

            return false;
        }

        $ppOrder = (new TCCaptureDecline())->execute($this, (new TCCapturePending(false))->execute($this, $ppOrder));
        switch ($eventType) {
            case EventType::CAPTURE_COMPLETED:
                if ($this->isValidOrderState($ppOrder, OrderStatus::STATUS_COMPLETED)) {
                    $this->helper->capturePayment($shopOrder, $capture, $ppOrder, $this);
                } elseif ($this->isValidOrderState($ppOrder, OrderStatus::STATUS_DECLINED)) {
                    $this->helper->declinePayment($shopOrder, $capture, $this);
                }
                $this->onPaymentComplete($ppOrder);

                return true;
            case EventType::CAPTURE_DENIED:
            case EventType::CAPTURE_REVERSED:
                $this->helper->declinePayment($shopOrder, $capture, $this);
                $this->onPaymentComplete($ppOrder);

                return true;
        }

        $this->helper->logWrite(\LOGLEVEL_DEBUG, 'handleCaptureWebhook - event type '
            . $eventType . ' not supported');

        return false;
    }
}
