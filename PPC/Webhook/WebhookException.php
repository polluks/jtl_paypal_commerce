<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Webhook;

use Exception;

/**
 * Class WebhookException
 * @package Plugin\jtl_paypal_commerce\PPC\Webhook
 */
class WebhookException extends Exception
{
}
