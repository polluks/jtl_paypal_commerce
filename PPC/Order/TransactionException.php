<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order;

use RuntimeException;

/**
 * Class TransactionException
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class TransactionException extends RuntimeException
{
}
