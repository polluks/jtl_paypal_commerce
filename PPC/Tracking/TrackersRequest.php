<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Tracking;

use Plugin\jtl_paypal_commerce\PPC\Request\AuthorizedRequest;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\SerializerInterface;

/**
 * Class TrackersRequest
 * @package Plugin\jtl_paypal_commerce\PPC\Tracking
 */
class TrackersRequest extends AuthorizedRequest
{
    /** @var JSON */
    protected $trackers;

    /**
     * TrackersRequest constructor
     */
    public function __construct(string $token, array $trackers)
    {
        $this->trackers = new JSON((object)['trackers' => $trackers]);

        parent::__construct($token);
    }

    /**
     * @inheritDoc
     */
    protected function initBody(): SerializerInterface
    {
        return $this->trackers;
    }

    /**
     * @inheritDoc
     */
    protected function getPath(): string
    {
        return '/v1/shipping/trackers-batch';
    }
}
