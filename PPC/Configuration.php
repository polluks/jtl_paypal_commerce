<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC;

use Exception;
use Illuminate\Support\Collection;
use JTL\DB\DbInterface;
use JTL\Plugin\Helper;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;

/**
 * Class Configuration
 * @package Plugin\jtl_paypal_commerce
 */
class Configuration
{
    /** @var static[] */
    protected static array $instance = [];

    /** @var DbInterface */
    protected DbInterface $db;

    /** @var PluginInterface */
    protected PluginInterface $plugin;

    /** @var Collection|null */
    protected static ?Collection $config = null;

    /** @var Collection[] */
    protected static array $items = [];

    /** @var string|null */
    private static ?string $authToken = null;

    private const PREFIX = 'jtl_paypal_commerce_';

    public const WORKING_MODE_SANDBOX    = 'sandbox';
    public const WORKING_MODE_PRODUCTION = 'production';
    public const CONSENT_ID              = self::PREFIX . 'consent';

    /**
     * Configuration constructor.
     * @param PluginInterface $plugin
     * @param DbInterface     $db
     */
    protected function __construct(PluginInterface $plugin, DbInterface $db)
    {
        $this->db     = $db;
        $this->plugin = $plugin;

        static::$instance[static::class] = $this;
    }

    /**
     * @param PluginInterface $plugin
     * @param DbInterface     $db
     * @return static
     */
    public static function getInstance(PluginInterface $plugin, DbInterface $db): static
    {
        return $instance[static::class] ?? new static($plugin, $db);
    }

    /**
     * @param bool $forceLoad
     * @return Collection
     */
    protected function loadConfig(bool $forceLoad = false): Collection
    {
        if (static::$config === null || $forceLoad) {
            static::$config = new Collection(Helper::getConfigByID($this->plugin->getID()));
        }

        return static::$config;
    }

    /**
     * @param Collection $config
     * @return Collection
     */
    protected function loadDefaults(Collection $config): Collection
    {
        return $config;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return self::PREFIX;
    }

    /**
     * @param string $name
     * @param null   $default
     * @return string|null
     */
    public function getConfigItem(string $name, $default = null): ?string
    {
        return $this->loadDefaults($this->loadConfig())->get($name, $default);
    }

    /**
     * @param string|null $prefix
     * @return Collection
     */
    public function getConfigItemsByPrefix(?string $prefix = null): Collection
    {
        $prefix = $prefix ?? $this->getPrefix();

        if (!isset(static::$items[$prefix])) {
            static::$items[$prefix] = $this->loadDefaults($this->loadConfig())->filter(
                static function (string $value, string $key) use ($prefix) {
                    return \str_starts_with($key, $prefix);
                }
            )->mapWithKeys(static function (string $value, string $key) use ($prefix) {
                return [\str_replace($prefix, '', $key) => $value];
            });
        }

        return static::$items[$prefix];
    }

    /**
     * @param string      $name
     * @param null        $default
     * @param string|null $prefix
     * @return string|null
     */
    public function getPrefixedConfigItem(string $name, $default = null, ?string $prefix = null): ?string
    {
        $prefix      = $prefix ?? $this->getPrefix();
        $configItems = $this->getConfigItemsByPrefix($prefix);

        return $configItems[$name] ?? $default;
    }

    /**
     * @return void
     */
    private function flushConfigCache(): void
    {
        Shop::Container()->getCache()->flushTags([\CACHING_GROUP_PLUGIN . '_' . $this->plugin->getID()]);
    }

    /**
     * @param array       $items
     * @param string|null $prefix
     */
    public function saveConfigItems(array $items, ?string $prefix = null): void
    {
        self::$config = null;
        $pluginID     = $this->plugin->getID();
        $prefix       = $prefix ?? $this->getPrefix();

        foreach ($items as $key => $value) {
            $this->db->delete('tplugineinstellungen', ['kPlugin', 'cName'], [$pluginID, $prefix . $key]);
            if (isset(static::$items[$prefix])) {
                static::$items[$prefix]->forget($key);
            }
            if ($value !== '') {
                $this->db->insert('tplugineinstellungen', (object)[
                    'kPlugin' => $this->plugin->getID(),
                    'cName'   => $prefix . $key,
                    'cWert'   => \is_array($value) ? \implode(',', $value) : $value,
                ]);
                if (isset(static::$items[$prefix])) {
                    static::$items[$prefix]->put($key, $value);
                }
            }
        }
        $this->flushConfigCache();
    }

    /**
     * @param string[]    $items
     * @param string|null $prefix
     */
    public function deleteConfigItems(array $items, ?string $prefix = null): void
    {
        self::$config = null;
        $prefix       = $prefix ?? $this->getPrefix();

        foreach ($items as $value) {
            $this->db->delete('tplugineinstellungen', ['kPlugin', 'cName'], [$this->plugin->getID(), $prefix . $value]);
        }
        $this->flushConfigCache();
    }

    /**
     * @return bool
     */
    public function isAuthConfigured(): bool
    {
        return $this->getClientID() !== ''
            && $this->getClientSecret() !== '';
    }

    /**
     * @return string
     */
    public function getWorkingMode(): string
    {
        return $this->getPrefixedConfigItem('clientType', self::WORKING_MODE_SANDBOX);
    }

    /**
     * @param string $workingMode
     */
    public function setWorkingMode(string $workingMode): void
    {
        $this->saveConfigItems(['clientType' => $workingMode]);
        $this->clearAuthToken();
    }

    /**
     * @param string|null $workingMode
     * @return string
     */
    public function getClientID(?string $workingMode = null): string
    {
        $workingMode = $workingMode ?? $this->getWorkingMode();

        return $this->getPrefixedConfigItem('clientID_' . $workingMode, '');
    }

    /**
     * @param string $clientID
     * @param string|null $workingMode
     */
    public function setClientID(string $clientID, ?string $workingMode = null): void
    {
        $workingMode = $workingMode ?? $this->getWorkingMode();
        $configItem  = 'clientID_' . $workingMode;
        if ($clientID === '') {
            $this->deleteConfigItems([$configItem]);
        } else {
            $this->saveConfigItems([$configItem => $clientID]);
        }
        $this->clearAuthToken();
    }

    /**
     * @param string|null $workingMode
     * @return string
     */
    public function getClientSecret(?string $workingMode = null): string
    {
        $workingMode = $workingMode ?? $this->getWorkingMode();

        return $this->getPrefixedConfigItem('clientSecret_' . $workingMode, '');
    }

    /**
     * @param string $clientSecret
     * @param string|null $workingMode
     */
    public function setClientSecret(string $clientSecret, ?string $workingMode = null): void
    {
        $workingMode = $workingMode ?? $this->getWorkingMode();
        $configItem  = 'clientSecret_' . $workingMode;
        if ($clientSecret === '') {
            $this->deleteConfigItems([$configItem]);
        } else {
            $this->saveConfigItems([$configItem => $clientSecret]);
        }
        $this->clearAuthToken();
    }

    /**
     * @return string
     */
    public function getAuthToken(): string
    {
        if (self::$authToken === null) {
            self::$authToken = ($this->db->selectSingleRow(
                'tplugineinstellungen',
                ['kPlugin', 'cName'],
                [$this->plugin->getID(), self::PREFIX . 'authToken']
            ))->cWert ?? '';
        }

        return self::$authToken;
    }

    /**
     * @param string $authToken
     */
    public function setAuthToken(string $authToken): void
    {
        if (self::$authToken === $authToken) {
            return;
        }

        self::$authToken = $authToken;

        $pluginId = $this->plugin->getID();
        $name     = self::PREFIX . 'authToken';
        $this->db->delete('tplugineinstellungen', ['kPlugin', 'cName'], [$pluginId, $name]);
        $this->db->insert('tplugineinstellungen', (object)[
            'kPlugin' => $pluginId,
            'cName'   => $name,
            'cWert'   => $authToken,
        ]);
    }

    /**
     * @return void
     */
    public function clearAuthToken(): void
    {
        $this->db->delete(
            'tplugineinstellungen',
            ['kPlugin', 'cName'],
            [$this->plugin->getID(), self::PREFIX . 'authToken']
        );
        self::$authToken = null;
        Token::inValidate();
    }

    /**
     * @param string $nonce
     * @param string|null $workingMode
     */
    public function setNonce(string $nonce, ?string $workingMode = null): void
    {
        $workingMode = $workingMode ?? $this->getWorkingMode();
        $configItem  = 'nonce_' . $workingMode;
        if ($nonce === '') {
            $this->deleteConfigItems([$configItem]);
        } else {
            $this->saveConfigItems([$configItem => $nonce]);
        }
    }

    /**
     * @param string|null $workingMode
     * @return string
     */
    public function getNonce(?string $workingMode = null): string
    {
        $workingMode = $workingMode ?? $this->getWorkingMode();

        return $this->getPrefixedConfigItem('nonce_' . $workingMode, '');
    }

    /**
     * @param string|null $singleSelection
     * @param array|null  $exclude
     * @return array
     * @throws Exception
     */
    public function mapBackendSettings(string $singleSelection = null, array $exclude = null): array
    {
        $storedConfig    = $this->getConfigItemsByPrefix()->toArray();
        $workingMode     = $this->getWorkingMode();
        $defaultSettings = BackendUIsettings::getDefaultSettings();
        $sections        = BackendUIsettings::BACKEND_SETTINGS_SECTIONS;
        $panels          = new Collection(BackendUIsettings::BACKEND_SETTINGS_PANELS);
        $settingSections = [];
        $settings        = $defaultSettings->map(static function ($item, $key) use ($storedConfig, $workingMode) {
            $wmProp = $key.'_'.$workingMode;
            if (isset($storedConfig[$wmProp])) {
                $item['value'] = $storedConfig[$wmProp];
            }
            if (isset($storedConfig[$key])) {
                $item['value'] = $storedConfig[$key];
            }

            return $item;
        });

        if (isset($singleSelection)) {
            $settingSections[$singleSelection]['settings'] = $settings
                ->filter(static function ($item) use ($singleSelection) {
                    return $item['section'] === $singleSelection;
                })->sortBy(static function ($item) {
                    return $item['sort'];
                })->toArray();
            $settingSections[$singleSelection]['heading']  = \__($singleSelection);

            return $settingSections;
        }

        foreach ($sections as $section) {
            if (isset($exclude) && \in_array($section, $exclude, true)) {
                continue;
            }
            $settingSections[$section]['settings'] = $settings->filter(static function ($item) use ($section) {
                return $item['section'] === $section;
            })->sortBy(static function ($item) {
                return $item['sort'];
            })->toArray();
            $settingSections[$section]['heading']  = \__($section);
        }

        $result = [];
        $panels = $panels->sortBy(static function ($item) {
            return $item === BackendUIsettings::BACKEND_SETTINGS_PANEL_GENERAL ? 0 : 1;
        })->toArray();
        foreach ($panels as $panel) {
            foreach ($settingSections as $sectionKey => $section) {
                foreach ($section['settings'] as $settingName => $setting) {
                    if ($setting['panel'] === $panel) {
                        $result[$panel][$sectionKey]['settings'][$settingName] = $setting;
                        $result[$panel][$sectionKey]['heading']                = \__($sectionKey);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param string|null $singleSelection
     * @param array|null  $exclude
     * @param array|null  $include
     * @return array
     */
    public function mapFrontendSettings(
        string $singleSelection = null,
        array $exclude = null,
        array $include = null
    ): array {
        $configs      = $this->getConfigItemsByPrefix()->toArray();
        $mappedConfig = [];

        foreach ($configs as $key => $val) {
            foreach (BackendUIsettings::BACKEND_SETTINGS_SECTIONS as $section) {
                //safety first, exclude credentials section by default
                if (BackendUIsettings::BACKEND_SETTINGS_SECTION_CREDENTIALS === $section) {
                    continue;
                }
                if (isset($exclude) && \in_array($section, $exclude, true)) {
                    continue;
                }
                if (isset($include) && !\in_array($section, $include, true)) {
                    continue;
                }
                if (\str_contains($key, $section)) {
                    $property                          = \str_replace($section . '_', '', $key);
                    $mappedConfig[$section][$property] = $val;
                }
            }
        }

        // static configs, no user interaction
        $mappedConfig[BackendUIsettings::BACKEND_SETTINGS_SECTION_SMARTPAYMENTBTNS]['label'] = 'buynow';

        return $mappedConfig[$singleSelection] ?? $mappedConfig;
    }

    /**
     * @param array  $config
     * @param string $scope
     * @return bool
     */
    public function checkComponentVisibility(array $config, string $scope): bool
    {
        $pageConfig = 'showIn' . \ucfirst($scope);

        /** @noinspection IfReturnReturnSimplificationInspection */
        if (!isset($config['activate'], $config[$pageConfig]) ||
            $config['activate'] === 'N' || $config[$pageConfig] === 'N') {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getWebhookId(): string
    {
        return $this->getPrefixedConfigItem('webhook_id', '');
    }

    /**
     * @return string
     */
    public function getWebhookUrl(): string
    {
        return $this->getPrefixedConfigItem('webhook_url', '');
    }

    /**
     * @param string $webhookId
     */
    public function setWebhookId(string $webhookId): void
    {
        $this->saveConfigItems(['webhook_id' => $webhookId]);
    }

    /**
     * @param string $webhookUrl
     */
    public function setWebhookUrl(string $webhookUrl): void
    {
        $this->saveConfigItems(['webhook_url' => $webhookUrl]);
    }

    /**
     * remove webhook-ID
     */
    public function removeWebhookId(): void
    {
        $this->deleteConfigItems(['webhook_id']);
    }

    /**
     * remove webhook-URL
     */
    public function removeWebhookUrl(): void
    {
        $this->deleteConfigItems(['webhook_url']);
    }
}
