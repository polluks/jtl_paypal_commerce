<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce;

use JTL\Checkout\Lieferadresse;
use JTL\Checkout\OrderHandler;
use JTL\Customer\Customer;
use JTL\Customer\Registration\Form as RegistrationForm;
use JTL\Helpers\Form;
use JTL\Router\Controller\CheckoutController;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Shopsetting;

/**
 * Class LegacyHelper
 * @package Plugin\jtl_paypal_commerce
 */
class LegacyHelper
{
    /** @var CheckoutController|null */
    private static ?CheckoutController $checkoutController = null;

    /**
     * @return CheckoutController
     */
    private static function getCheckoutController(): CheckoutController
    {
        if (self::$checkoutController === null) {
            self::$checkoutController = new CheckoutController(
                Shop::Container()->getDB(),
                Shop::Container()->getCache(),
                Shop::getRouter()->getState(),
                Shopsetting::getInstance()->getAll(),
                Shop::Container()->getAlertService()
            );
            self::$checkoutController->init();
        }

        return self::$checkoutController;
    }

    /**
     * @return string
     */
    public static function baueBestellnummer(): string
    {
        $orderHandler = new OrderHandler(
            Shop::Container()->getDB(),
            Frontend::getCustomer(),
            Frontend::getCart()
        );

        return $orderHandler->createOrderNo();
    }

    /**
     * @param int   $shippingMethod
     * @param array $formValues
     * @param bool  $bMsg
     * @return bool
     */
    public static function pruefeVersandartWahl(int $shippingMethod, array $formValues, bool $bMsg = true): bool
    {
        return self::getCheckoutController()->checkShippingSelection($shippingMethod, $formValues, $bMsg);
    }

    /**
     * @param array $data
     * @return int|null
     */
    public static function pruefeZahlungsartwahlStep(array $data): ?int
    {
        return self::getCheckoutController()->checkStepPaymentMethodSelection($data);
    }

    /**
     * @param array $missingData
     * @return int
     */
    public static function angabenKorrekt(array $missingData): int
    {
        return Form::hasNoMissingData($missingData);
    }

    /**
     * @param array $data
     * @return Lieferadresse
     */
    public static function getLieferdaten(array $data): Lieferadresse
    {
        return Lieferadresse::createFromPost($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function checkLieferFormularArray(array $data): array
    {
        return (new RegistrationForm())->checkLieferFormularArray($data);
    }

    /**
     * @param array $data
     * @param int   $customerAccount
     * @param int   $htmlentities
     * @return Customer
     */
    public static function getKundendaten(array $data, int $customerAccount, int $htmlentities = 1): Customer
    {
        return (new RegistrationForm())->getCustomerData($data, (bool)$customerAccount, (bool)$htmlentities);
    }

    /**
     * @param array $data
     * @param int   $customerAccount
     * @param int   $checkpass
     * @return array
     */
    public static function checkKundenFormularArray(array $data, int $customerAccount, int $checkpass = 1): array
    {
        return (new RegistrationForm())->checkKundenFormularArray($data, (bool)$customerAccount, (bool)$checkpass);
    }
}
