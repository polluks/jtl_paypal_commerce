const standaloneButtonTemplate = ({id, fundingSource, layout}) =>
`<div id="${id}" class="${layout === 'horizontal' ? 'col-md-6 ' : ''}ppc-standalone-buttons"></div>`
