<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use Exception;
use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Customer\Customer;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Transaction;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;

/**
 * Class ACDCFrontend
 * @package Plugin\jtl_paypal_commerce\frontend
 */
class ACDCFrontend extends AbstractPaymentFrontend
{
    /**
     * @inheritDoc
     */
    public function renderProductDetailsPage(Customer $customer, Cart $cart, ?Artikel $product): void
    {
        // no action at product details page
    }

    /**
     * @inheritDoc
     */
    public function renderCartPage(Customer $customer, Cart $cart): void
    {
        // no action at product details page
    }

    /**
     * @inheritDoc
     */
    public function renderAddressPage(Customer $customer, Cart $cart): void
    {
        // no action at address page
    }

    /**
     * @inheritDoc
     */
    public function renderShippingPage(Customer $customer, Cart $cart): void
    {
        if (!$this->paymentMethod->isValid($customer, $cart)) {
            return;
        }

        /** @var Configuration $config */
        Transaction::instance()->clearAllTransactions();
        $acdcMethod = $this->paymentMethod->getMethod();
        $config     = PPCHelper::getConfiguration($this->plugin);
        $sca        = $config->getPrefixedConfigItem(
            BackendUIsettings::BACKEND_SETTINGS_SECTION_ACDCDISPLAY . '_activate3DSecure',
            'Y'
        ) !== 'Y' ? 'N' : $config->getPrefixedConfigItem(
            BackendUIsettings::BACKEND_SETTINGS_SECTION_ACDCDISPLAY . '_mode3DSecure',
            'SCA_WHEN_REQUIRED'
        );
        try {
            $localization = $this->plugin->getLocalization();
            \pq('#' . $acdcMethod->getModuleID())
                ->append($this->smarty
                    ->assign('acdcModuleId', $acdcMethod->getModuleID())
                    ->assign('acdcPaymentId', $acdcMethod->getMethodID())
                    ->assign('acdcBNCode', MerchantCredentials::BNCODE_ACDC)
                    ->assign('acdcSCAMode', $sca)
                    ->assign('acdcMethodName', $this->paymentMethod->getLocalizedPaymentName())
                    ->assign('acdcGeneralError', \sprintf(
                        Handler::getBackendTranslation('Die Zahlungsmethode %s ist nicht verfügbar.'),
                        $this->paymentMethod->getLocalizedPaymentName()
                    ))
                    ->assign('acdc3DSError', $localization->getTranslation('acdc_3dserror_occured'))
                    ->assign('msg_acdc_invalid_input', $localization->getTranslation('acdc_invalid_input'))
                    ->assign('msg_acdc_potentially_valid', $localization->getTranslation('acdc_potentially_valid'))
                    ->assign('customer', $customer)
                    ->assign('acdcImagePath', $this->plugin->getPaths()->getFrontendURL() . 'img')
                    ->fetch($acdcMethod->getAdditionalTemplate()));
        } catch (Exception) {
            $logger = Shop::Container()->getLogService();
            $logger->addRecord(
                $logger::ERROR,
                'phpquery rendering failed: ACDCFrontend::renderShippingPage()'
            );

            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function renderConfirmationPage(int $paymentId, Customer $customer, Cart $cart): void
    {
        if ($this->paymentMethod->getMethod()->getMethodID() !== $paymentId) {
            return;
        }

        $ppcOrderId = $this->paymentMethod->createPPOrder(
            $customer,
            $cart,
            'paypalACDC',
            AppContext::SHIPPING_PROVIDED,
            AppContext::PAY_CONTINUE,
            $this->paymentMethod->getBNCode()
        );
        $ppOrder    = $this->paymentMethod->getPPOrder($ppcOrderId);
        if ($ppOrder === null) {
            // ToDo: Frontend error message
            Helper::redirectAndExit($this->paymentMethod->getPaymentCancelURL());
            exit();
        }
    }
}
