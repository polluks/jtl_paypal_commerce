<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\adminmenu;

use JTL\Backend\NotificationEntry;
use JTL\DB\DbInterface;
use JTL\IO\IOResponse;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;

/**
 * Class Handler
 * @package Plugin\jtl_paypal_commerce\adminmenu
 */
class Handler
{
    /** @var PluginInterface */
    private PluginInterface $plugin;

    /** @var DbInterface */
    private DbInterface $db;

    /**
     * Handler constructor.
     * @param PluginInterface  $plugin
     * @param DbInterface|null $db
     */
    public function __construct(PluginInterface $plugin, ?DbInterface $db = null)
    {
        $this->plugin = $plugin;
        $this->db     = $db ?? Shop::Container()->getDB();
    }

    /**
     * @return void
     */
    public function smarty(): void
    {
    }

    /**
     * @param string $tplElement
     * @return IOResponse
     * @noinspection PhpUnused
     */
    public function handleAjax(string $tplElement): IOResponse
    {
        $results             = [
            NotificationEntry::TYPE_DANGER  => '<i class="fa fa-times text-danger"></i>',  // red cross
            NotificationEntry::TYPE_WARNING => '<i class="fa fa-check text-warning"></i>', // orange hook
            NotificationEntry::TYPE_NONE    => '<i class="fa fa-check text-success"></i>', // green hook
        ];
        $resultWrap          = '<span data-html="true" data-toggle="tooltip" data-placement="left"
                title="" data-original-title="%s">%s</span>';
        $displayResult       = $results[NotificationEntry::TYPE_DANGER];
        [$paymentID, $tplID] = \explode('_', $tplElement);
        $infoCheck           = new TabInfoChecks($this->plugin);

        if (($tplID === 'payment-linked')) {
            $displayResult = $infoCheck->isShippmentLinked((int)$paymentID, $results, $resultWrap);
        }
        if (($tplID === 'ppc-connectable')) {
            $displayResult = $infoCheck->getConnectionInfo((int)$paymentID, $results, $resultWrap);
        }
        $response = new IOResponse();
        $response->assignDom(
            $paymentID.'_'.$tplID,
            'innerHTML',
            $displayResult
        );

        return $response;
    }

    /**
     * @param string $query
     * @param int    $limit
     * @return string
     */
    public function handleCarrierMapping(string $query, int $limit): string
    {
        $results = $this->db->getObjects(
            'SELECT DISTINCT cLogistiker AS cName
                FROM tbestellung
                WHERE cLogistiker LIKE :search' .
            ($limit > 0 ? ' LIMIT ' . $limit : ''),
            [
                'search' => '%' . $query . '%'
            ]
        );

        try {
            return \json_encode($results, \JSON_THROW_ON_ERROR);
        } catch (\JsonException) {
            return '';
        }
    }

    /**
     * @param int    $methodId
     * @param string $txnId
     * @return IOResponse
     */
    public function handleOrderState(int $methodId, string $txnId): IOResponse
    {
        $infos   = [
            OrderStatus::STATUS_COMPLETED => '<i class="fa fa-check text-success"></i>', // green hook
            OrderStatus::STATUS_PENDING   => '<i class="fa fa-clock text-warning"></i>', // orange clock
            OrderStatus::STATUS_DECLINED  => '<i class="fa fa-ban text-danger"></i>',    // red ban
            OrderStatus::STATUS_APPROVED  => '<i class="fa fa-thumbs-up text-info"></i>',
            'UNKNOWN'                     => '<i class="fa fa-question-circle text-info"></i>', // grey question mark
        ];
        $wrapper = '<span data-html="true" data-toggle="tooltip" data-placement="left"
                title="" data-original-title="%s">%s</span>';

        $response  = new IOResponse();
        $infoCheck = new TabInfoChecks($this->plugin);
        $state     = null;
        $response
            ->assignDom($txnId, 'innerHTML', $infoCheck->getOrderState($methodId, $txnId, $state, $infos, $wrapper))
            ->assignDom('reload-' . $txnId, 'disabled', false)
            ->assignDom('delete-' . $txnId, 'disabled', false)
            ->assignDom('apply-' . $txnId, 'disabled', !\in_array($state ?? '', [
                OrderStatus::STATUS_COMPLETED,
                OrderStatus::STATUS_DECLINED
            ], true));

        return $response;
    }
}
